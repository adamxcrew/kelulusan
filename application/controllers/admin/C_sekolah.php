<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_sekolah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['my', 'debug']);
        $this->load->library('form_validation');
        $this->load->model('M_admin', 'model');
        is_logged_in();
    }

    public function index()
    {
        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();
        $data['sekolah'] = $this->model->get('tbl_school');

        $this->form_validation->set_rules('npsn', 'NPSN', 'trim|required|numeric');
        $this->form_validation->set_rules('nama_sekolah', 'Nama Sekolah', 'trim|required');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'trim|required');
        $this->form_validation->set_rules('nama_kepsek', 'Kepala Sekolah', 'trim|required');
        $this->form_validation->set_rules('nip', 'NIP', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/v_sekolah', $data);
        } else {
            $sekolah = [
                'npsn'           => $this->input->post('npsn', true),
                'nama_sekolah'   => $this->input->post('nama_sekolah', true),
                'kabupaten'      => $this->input->post('kabupaten', true),
                'kepala_sekolah' => $this->input->post('nama_kepsek', true),
                'nip'            => $this->input->post('nip', true)
            ];
            // cek apakah sudah ada data di db ?
            if (empty($this->db->count_all('tbl_school'))) {
                // jika belum ada data, simpan
                $this->model->save('tbl_school', $sekolah);
            } else {
                // Jika sudah ada data, update
                $first_row = $this->model->get('tbl_school')->first_row();
                $this->model->update('tbl_school', $sekolah, ['id' => $first_row->id]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Data sekolah telah disimpan!</div>');
            redirect('admin/sekolah');
        }
    }
}
