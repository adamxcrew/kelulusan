<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_jadwal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['my', 'debug']);
        $this->load->library('form_validation');
        $this->load->model('M_admin', 'model');
        is_logged_in();
    }

    public function index()
    {
        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();
        $data['jadwal'] = $this->model->get('tbl_jadwal');

        $this->form_validation->set_rules('start_time', 'Tanggal Mulai', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/v_jadwal', $data);
        } else {
            $jadwal = date('Y-m-d H:i', strtotime($this->input->post('jadwal', true)));
            // cek apakah jadwal sudah pernah di set ?
            if (empty($this->db->count_all('tbl_jadwal'))) {
                // jika belum ada data, simpan
                $this->model->save('tbl_jadwal', ['start_date' => $jadwal]);
            } else {
                // Jika sudah ada data, update
                $first_row = $this->model->get('tbl_jadwal')->first_row();
                $this->model->update('tbl_jadwal', ['start_date' => $jadwal], ['id' => $first_row->id]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Jadwal pengumuman telah disimpan!</div>');
            redirect('admin/pengaturan/jadwal');
        }
    }
}
