<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['my', 'debug']);
        $this->load->library('form_validation');
        $this->load->model('M_admin', 'model');
        is_logged_in();
    }

    public function index()
    {
        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();
        $data['data_siswa'] = $this->_data_siswa();
        $data['counter'] = $this->_counter();
        $this->load->view('admin/index', $data);
    }

    private function _data_siswa()
    {
        $query = "SELECT `tbl_student`.id, `tbl_student`.name, `tbl_student`.is_paid, `tbl_scores`.*
                    FROM `tbl_student`
              INNER JOIN `tbl_scores`
                      ON `tbl_student`.id = `tbl_scores`.student_id";

        if ($this->model->setQuery($query)->num_rows() > 0) {
            return $this->model->setQuery($query)->result_array();
        }
        return false;
    }

    public function schedule()
    {
        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();
        $data['timer'] = $this->model->get('tbl_timer');

        $this->form_validation->set_rules('start_time', 'Tanggal Mulai', 'trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/v_timer', $data);
        } else {
            $timer = date('Y-m-d H:i:s', strtotime($this->input->post('timer', true)));
            // cek apakah timer sudah pernah di set ?
            if (empty($this->db->count_all('tbl_timer'))) {
                // jika belum ada data, simpan
                $this->model->save('tbl_timer', ['start_date' => $timer]);
            } else {
                // Jika sudah ada data, update
                $first_row = $this->model->get('tbl_timer')->first_row();
                $this->model->update('tbl_timer', ['start_date' => $timer], ['id' => $first_row->id]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Jadwal pengumuman telah disimpan!</div>');
            redirect('admin/timer');
        }
    }

    private function _counter()
    {
        $web_browser = $_SERVER['HTTP_USER_AGENT'];
        $chrome = '/Chrome/';
        $firefox = '/Firefox/';
        $ie = '/IE/';
        if (preg_match($chrome, $web_browser))
            $d = "Chrome/Opera";
        if (preg_match($firefox, $web_browser))
            $d = "Firefox";
        if (preg_match($ie, $web_browser))
            $d = "IE";

        // untuk mengambil informasi dari pengunjung
        $ipaddress = $_SERVER['REMOTE_ADDR'] . "";
        $web_browser = $d;
        $tanggal = date('Y-m-d');
        $kunjungan = 1;
        // Daftarkan Kedalam Session Lalu Simpan Ke Database
        if (!$this->session->set_userdata('counterdb')) {
            $this->session->set_userdata('counterdb', $ipaddress);
            $hit = [
                'tanggal' => $tanggal,
                'ip_address' => $ipaddress,
                'pengunjung' => $kunjungan,
                'browser' => $d
            ];
            $this->db->insert('tbl_counter', $hit);
        }

        // Hitung Jumlah Visitor
        $kemarin = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
        $data['hari_ini'] = $this->db->query('SELECT SUM(pengunjung) AS hari_ini FROM tbl_counter WHERE tanggal ="' .  date("Y-m-d") . '"')->result_array();
        $data['kemarin'] = $this->db->query('SELECT SUM(pengunjung) AS kemarin FROM tbl_counter WHERE tanggal="' . $kemarin . '"')->result_array();
        $data['total'] = $this->db->query('SELECT SUM(pengunjung) AS total FROM tbl_counter')->result_array();

        return $data;
    }
}
