<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_laporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin', 'model');
        $this->load->library(['fpdf181/fpdf']);
        $this->load->helper(['my', 'debug']);
        is_logged_in();
        if ($this->session->userdata('role_id') != 1) redirect('login');
    }

    public function index()
    {
        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();
        if ($this->input->get('filter') && !empty($this->input->get('filter'))) {
            $filter = $this->input->get('filter');

            if ($filter == 1) { // filter by lulus
                $url_cetak = 'admin/laporan/cetak?filter=1';
                $data['keterangan'] = "Data Siswa Lulus";
                $data['data_siswa'] = $this->_filter(1);
            } elseif ($filter == 2) {
                $url_cetak = 'admin/laporan/cetak?filter=2';
                $data['keterangan'] = "Data Siswa Tidak Lulus";
                $data['data_siswa'] = $this->_filter(0);
            } else {
                $url_cetak = 'admin/laporan/cetak';
                $data['keterangan'] = "Semua Data Siswa";
                $data['data_siswa'] = $this->_data_siswa();
            }
        } else { // Jika tidak klik tombol filter data, tampilkan semua
            $url_cetak = 'admin/laporan/cetak';
            $data['keterangan'] = "Semua Data Siswa";
            $data['data_siswa'] = $this->_data_siswa();
        }
        $data['url_cetak'] = site_url($url_cetak);
        $this->load->view('admin/siswa/v_laporan', $data);
    }

    private function _data_siswa()
    {
        $query = "SELECT `tbl_student`.*,
                         `tbl_scores`.*,
                         `tbl_users`.role_id, `tbl_users`.username
                    FROM `tbl_student`
              INNER JOIN `tbl_scores`
                      ON `tbl_student`.id = `tbl_scores`.student_id
              INNER JOIN `tbl_users`
                      ON `tbl_student`.id = `tbl_users`.username
                ORDER BY `tbl_student`.name ASC";

        if ($this->model->setQuery($query)->num_rows() > 0) {
            return $this->model->setQuery($query)->result_array();
        }
        return false;
    }

    private function _filter($n)
    {
        $uri = ($n != 1) ? "tidak lulus" : "lulus";

        $query = "SELECT `tbl_student`.*,
                         `tbl_scores`.*,
                         `tbl_users`.role_id, `tbl_users`.username
                    FROM `tbl_student`
              INNER JOIN `tbl_scores`
                      ON `tbl_student`.id = `tbl_scores`.student_id
              INNER JOIN `tbl_users`
                      ON `tbl_student`.id = `tbl_users`.username
                   WHERE `tbl_scores`.is_pass = '$uri'
                ORDER BY `tbl_student`.name ASC";

        if ($this->model->setQuery($query)->num_rows() > 0) {
            return $this->model->setQuery($query)->result_array();
        }
        return false;
    }

    public function cetak()
    {
        if ($this->input->get('filter') && !empty($this->input->get('filter'))) {
            $filter = $this->input->get('filter');

            if ($filter == 2) { // filter by tidak lulus
                $this->_pdf(2);
            } elseif ($filter == 1) {
                $this->_pdf(1);
            } else {
                $this->_pdf();
            }
        } else { // Jika tidak klik tombol filter data, tampilkan semua
            $this->_pdf();
        }
    }

    private function _pdf($filter = null)
    {
        $pdf = new FPDF('l', 'mm', 'legal');
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(350, 7, 'SMP NEGERI 72 JAKARTA', 0, 1, "C");
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(350, 7, 'DAFTAR SISWA KELAS IX', 0, 1, 'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10, 7, '', 0, 1);

        if (empty($filter) || $filter == null) {
            $siswa = $this->_data_siswa();
            $ket = "Data Seluruh Siswa";
        } elseif ($filter == 1) {
            $siswa = $this->_filter(1);
            $ket = "Data Siswa yang Lulus";
        } elseif ($filter == 2) {
            $siswa = $this->_filter(0);
            $ket = "Data Siswa TIdak Lulus";
        }
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(30, 6, $ket, 0, 1, "L");

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(36, 6, 'NOMOR UJIAN', 1, 0);
        $pdf->Cell(60, 6, 'NAMA PESERTA', 1, 0);
        $pdf->Cell(20, 6, 'JK', 1, 0);
        $pdf->Cell(27, 6, 'TGL LAHIR', 1, 0);
        $pdf->Cell(30, 6, 'B.INDONESIA', 1, 0);
        $pdf->Cell(30, 6, 'B.INGGRIS', 1, 0);
        $pdf->Cell(30, 6, 'MATEMATIKA', 1, 0);
        $pdf->Cell(27, 6, 'IPA', 1, 0);
        $pdf->Cell(27, 6, 'TOTAL', 1, 0);
        $pdf->Cell(27, 6, 'RERATA', 1, 0);
        $pdf->Cell(30, 6, 'STATUS', 1, 1);
        $pdf->SetFont('Arial', '', 11);
        foreach ($siswa as $row) {
            if ($row['is_pass'] == "lulus") $status = "LULUS";
            else $status = "TIDAK LULUS";

            $pdf->Cell(36, 6, $row['student_id'], 1, 0);
            $pdf->Cell(60, 6, $row['name'], 1, 0);
            $pdf->Cell(20, 6, $row['gender'], 1, 0);
            $pdf->Cell(27, 6, date('d-m-Y', strtotime($row['birthday'])), 1, 0);
            $pdf->Cell(30, 6, $row['n_bindo'], 1, 0);
            $pdf->Cell(30, 6, $row['n_bing'], 1, 0);
            $pdf->Cell(30, 6, $row['n_mat'], 1, 0);
            $pdf->Cell(27, 6, $row['n_peminatan'], 1, 0);
            $pdf->Cell(27, 6, $row['n_total'], 1, 0);
            $pdf->Cell(27, 6, $row['n_rata'], 1, 0);
            $pdf->Cell(30, 6, $status, 1, 1);
        }
        $pdf->Output();
    }
}
