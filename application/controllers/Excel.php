<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Excel extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
        // $this->load->library('excel');
        $this->load->helper(['file', 'debug']);
    }

    public function index_()
    {
        $this->load->view('admin/siswa/v_index');
    }

    public function import()
    {
        if (isset($_FILES['upload_file']['name'])) {
            $fileName = time() . $_FILES['upload_file']['name'];

            $config['upload_path'] = './uploads/'; //buat folder dengan nama assets di root folder
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx|csv';
            $config['max_size'] = 10000;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('upload_file'))
                $this->upload->display_errors();

            $media         = $this->upload->data();
            $inputFileName = 'uploads/' . $media['file_name'];

            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader     = IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
            }

            $sheet         = $objPHPExcel->getSheet(0);
            $highestRow    = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= $highestRow; $row++) { //  Read a row of data into an array
                $rowData      = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $is_duplicate = $this->db->get_where('tbl_student', ['id' => $rowData[0][1]]);

                if ($is_duplicate->num_rows() > 0) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Import data gagal!</div>');
                    delete_files($media['file_path']);
                    redirect('admin/siswa');
                } else {
                    $tbl_student = [
                        'id'         => $rowData[0][1],
                        'name'       => strtoupper($rowData[0][2]),
                        'gender'     => $rowData[0][3],
                        'telp'       => $rowData[0][4],
                        'birthday'   => date('Y-m-d', ($rowData[0][5] - 25569) * 86400), // konversi ke timestamp unix lalu dikonversi menggunakan fungsi date php
                        // 'is_paid'    => trim($rowData[0][12]) == "lunas" ? 1 : 0,
                        'created_at' => time(),
                        'updated_at' => time()
                    ];

                    $tbl_scores = [
                        'student_id'  => $rowData[0][1],
                        'n_bindo'     => number_format($rowData[0][6], 2),
                        'n_bing'      => number_format($rowData[0][7], 2),
                        'n_mat'       => number_format($rowData[0][8], 2),
                        'n_peminatan' => number_format($rowData[0][9], 2),
                        'n_total'     => ($rowData[0][6] + $rowData[0][7] + $rowData[0][8] + $rowData[0][9]),
                        'n_rata'      => ($rowData[0][6] + $rowData[0][7] + $rowData[0][8] + $rowData[0][9]) / 4,
                        'is_pass'     => trim($rowData[0][12])
                    ];

                    $tbl_users = [
                        'role_id'    => 2,
                        'username'   => $rowData[0][1],
                        'password'   => password_hash(date("dmY", ($rowData[0][4] - 25569) * 86400), PASSWORD_DEFAULT),
                        'name'       => strtoupper($rowData[0][2]),
                        'is_active'  => 1,
                        'updated_at' => time()
                    ];
                    // dump($tbl_scores);
                    // die;
                    $this->db->insert('tbl_student', $tbl_student);
                    $this->db->insert('tbl_scores', $tbl_scores);
                    $this->db->insert('tbl_users', $tbl_users);
                }
            }
            delete_files($media['file_path']);
            $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Import data berhasil!</div>');
            redirect('admin/siswa');
        }
    }
}
