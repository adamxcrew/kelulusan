<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/icon-user.png') ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?= $this->session->userdata('username'); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU NAVIGATION</li>
            <?php
            $role_id = $this->session->userdata('role_id');
            $main_menu = $this->db->order_by('id', 'ASC')->get_where('tbl_menu', ['is_main_menu' => 0, 'is_active' => 1, 'role_id' => $role_id])->result_array();
            foreach ($main_menu as $main) {
                # Query untuk mencari sub-menu
                $sub_menu = $this->db->get_where('tbl_menu', ['is_main_menu' => $main['id'], 'is_active' => 1, 'role_id' => $role_id])->result_array();
                // periksa apakah ada sub-menu
                $uri = $this->uri->segment(2);
                $uri3 = $this->uri->segment(3);

                if ($sub_menu) {
                    ?>
                    <li class="treeview">
                        <a href="<?= $main['link']; ?>">
                            <i class="<?= $main['icon']; ?>"></i>
                            <span><?= ucwords($main['title']); ?></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <?php foreach ($sub_menu as $sub) : var_dump($sub['link']);
                                ?>
                                <li class='<?php if ($uri3 == $sub['title']) echo "active"; ?>'><a href="<?= $sub['link']; ?>"><i class="<?= $sub['icon']; ?>"></i> <?= ucwords($sub['title']); ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php
            } else {
                if ($uri == $main['title'] or $uri == null) echo "<li class='active'>";

                else echo "<li class=''>";
                echo anchor($main['link'], '<i class="' . $main['icon'] . '"> </i> ' . ucwords($main['title']));
                echo "</li>";
            }
        }
        ?>
            <li><a href="<?= site_url('logout'); ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">