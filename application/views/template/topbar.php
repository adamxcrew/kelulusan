</head>

<body class="skin-blue">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <a href="#" class="logo"><b><?php echo strtoupper($this->session->userdata('akses')); ?></b>LTE</a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="">
                            <a href="#" class="">
                                <?php $user = $users[0]['name']; ?>
                                <span class="hidden-xs"><i class="fa fa-user-circle"></i>&nbsp;<?= strtoupper($user); ?></span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?= site_url('logout'); ?>" class="btn btn-block btn-info">
                                <span class="hidden-xs"><i class="fa fa-sign-out"></i>&nbsp;Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->