<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SMPN 72 Jakarta | Cetak Hasil</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="http://static.smkdki.net/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="http://static.smkdki.net/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body onload="window.print();">
    <div class="wrapper">

        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <!-- <i class="fa fa-globe"></i> UN.SMKDKI.Net -->
                        <img src="http://static.smkdki.net/dist/img/banner.jpg">
                        <small class="pull-right">Tanggal: <?= date('d-m-Y'); ?></small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row">
                <h2 align="center">SURAT KETERANGAN</h2>
                <h3 align="center">Berdasarkan Hasil Rapat Dewan Guru</h3>
                <h3 align="center"><?= $sekolah[0]['nama_sekolah'] . " " . ucwords(strtolower($sekolah[0]['kabupaten'])); ?></h3>
                <h4 align="center">Peserta Didik :</h4><br />
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="center-block">
                    <div class="col-xs-10 col-xs-offset-1">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td><b><?= $siswa[0]['name']; ?></b></td>
                                </tr>
                                <tr>
                                    <td>Nomor Ujian</td>
                                    <td><b><?= $siswa[0]['student_id']; ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <h4 align="center">Dinyatakan :</h4>
                <h3 align="center">
                    <?php
                    if ($siswa[0]['is_pass'] == "lulus" or $siswa[0]['is_pass'] == 1) {
                        $is_pass = 'Lulus';
                    } else {
                        $is_pass = 'Tidak Lulus';
                    }
                    ?>
                    <font color="red">- <?= strtoupper($is_pass); ?> -</font>
                </h3>
                <h4 align="center">Dari Satuan Pendidikan <?= $sekolah[0]['nama_sekolah'] . " " . ucwords(strtolower($sekolah[0]['kabupaten'])); ?></h3>
                    <h4 align="center">Tahun Pelajaran 2018/2019</h3>
            </div>
            <div class="row">
                <!-- accepted payments column -->
                <!-- /.col -->
                <div class="col-xs-offset-6 col-xs-6">
                    <p align="center">
                        Jakarta, 29 Mei 2019<br />
                        Kepala
                        <?= $sekolah[0]['nama_sekolah']; ?> <br />
                        TTD
                        <br /><br /><br />
                        <b><?= $sekolah[0]['kepala_sekolah']; ?></b><br />
                        NIP. <br /><br /><br />
                    </p>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- ./wrapper -->
</body>

</html>