<?php $this->load->view('template/head'); ?>

<!--tambahkan custom css disini-->
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css" />

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">

            <?= $this->session->flashdata('message'); ?>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-university"></i>&nbsp;Data Sekolah</h3>
                </div>
                <div class="box-body">
                    <?php if ($sekolah->num_rows() < 1) : ?>
                        <div class="well well-sm">
                            <h3><i class="fa fa-info-circle"></i>&nbsp;sekolah pengumuman belum diatur!</h3>
                        </div>
                    <?php else : ?>
                        <?php $sekolah = $sekolah->result_array(); ?>
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <tr>
                                    <th width="200px">NPSN</th>
                                    <td><?= $sekolah[0]['npsn']; ?></td>
                                </tr>
                                <tr>
                                    <th>Nama Sekolah</th>
                                    <td><?= $sekolah[0]['nama_sekolah']; ?></td>
                                </tr>
                                <tr>
                                    <th>Kota/Kabupaten</th>
                                    <td><?= $sekolah[0]['kabupaten']; ?></td>
                                </tr>
                                <tr>
                                    <th>Nama Kepala Sekolah</th>
                                    <td><?= $sekolah[0]['kepala_sekolah']; ?></td>
                                </tr>
                                <tr>
                                    <th>NIP Kepala Sekolah</th>
                                    <?php $NIP = empty($sekolah[0]['NIP']) ? "-" : $sekolah[0]['NIP']; ?>
                                    <td><?= $NIP; ?></td>
                                </tr>
                            </table>
                        </div>
                    <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-xs-12 -->
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-university"></i>&nbsp;Data Sekolah</h3>
                </div>
                <div class="box-body">
                    <?= form_open('admin/sekolah', ['class' => 'form-horizontal']); ?>
                    <div class="form-group">
                        <label for="npsn" class="control-label col-sm-4">NPSN :</label>
                        <div class="col-sm-8">
                            <?= form_input('npsn', set_value('npsn'), ['class' => 'form-control', 'id' => 'npsn', 'placeholder' => 'Nomor Pokok Sekolah Nasional']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama_sekolah" class="control-label col-sm-4">Nama Sekolah :</label>
                        <div class="col-sm-8">
                            <?= form_input('nama_sekolah', set_value('nama_sekolah'), ['class' => 'form-control', 'id' => 'nama_sekolah', 'placeholder' => 'Nama Sekolah']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kabupaten" class="control-label col-sm-4">Kota/Kabupaten :</label>
                        <div class="col-sm-8">
                            <?= form_input('kabupaten', set_value('kabupaten'), ['class' => 'form-control', 'id' => 'kabupaten', 'placeholder' => 'Kota atau Kabupaten']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama_kepsek" class="control-label col-sm-4">Nama Kepala Sekolah :</label>
                        <div class="col-sm-8">
                            <?= form_input('nama_kepsek', set_value('nama_kepsek'), ['class' => 'form-control', 'id' => 'nama_kepsek', 'placeholder' => 'Nama Kepala Sekolah']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nip" class="control-label col-sm-4">NIP Kepala Sekolah :</label>
                        <div class="col-sm-8">
                            <?= form_input('nip', set_value('nip'), ['class' => 'form-control', 'id' => 'nip', 'placeholder' => 'Nomor Induk Kepala Sekolah']); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-xs-12 -->
    </div>
    <!-- /.row -->

</section><!-- /.content -->

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->

<?php $this->load->view('template/foot'); ?>