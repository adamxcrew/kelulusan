<?php $this->load->view('template/head'); ?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <!-- <h1>
        Dashboard
        <small>Version 1.19</small>
    </h1> -->
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Data Siswa</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo form_open('admin/siswa/add', ['class' => 'form-horizontal']); ?>
                <div class="box-body">
                    <?php echo $this->session->flashdata('message'); ?>
                    <!-- Nomor Peserta -->
                    <div class="form-group">
                        <label for="nomor_peserta" class="col-sm-2 control-label">Nomor Peserta</label>
                        <div class="col-sm-10">
                            <input type="text" name="nomor_peserta" class="form-control" id="nomor_peserta" placeholder="Nomor Peserta" value="<?php echo set_value('nomor_peserta'); ?>">
                            <?php echo form_error('nomor_peserta'); ?>
                        </div>
                    </div>
                    <!-- Nama Peserta -->
                    <div class="form-group">
                        <label for="nama_peserta" class="col-sm-2 control-label">Nama Peserta</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama_peserta" id="nama_peserta" placeholder="Nama Peserta" value="<?php echo set_value('nama_peserta'); ?>">
                            <?php echo form_error('nama_peserta'); ?>
                        </div>
                    </div>
                    <!-- Lulus -->
                    <div class="form-group">
                        <label for="gender" class="col-sm-2 control-label">Jenis Kelamin</label>
                        <div class="col-sm-3">
                            <select name="gender" id="gender" class="form-control">
                                <?php
                                $gender = ['L' => 'Laki-laki', 'P' => 'Perempuan'];
                                foreach ($gender as $jk => $val_jk) {
                                    echo "<option value='" . $jk . "'>" . $val_jk . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!-- Tanggal Lahir -->
                    <div class="form-group">
                        <label for="tgl_lahir" class="col-sm-2 control-label">Tanggal Lahir</label>
                        <div class="col-sm-3">
                            <input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" placeholder="Tanggal Lahir" value="<?php echo set_value('tgl_lahir'); ?>">
                            <?php echo form_error('tgl_lahir'); ?>
                        </div>
                    </div>
                    <!-- Nomor Telepon -->
                    <div class="form-group">
                        <label for="no_telp" class="col-sm-2 control-label">Nomor Telepon</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="Nomor Telepon" value="<?php echo set_value('no_telp'); ?>">
                            <?php echo form_error('no_telp'); ?>
                        </div>
                    </div>

                    <!-- Nilai Bindo -->
                    <div class="form-group">
                        <label for="n_bindo" class="col-sm-2 control-label">Nilai Bahasa Indonesia</label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" name="n_bindo" id="n_bindo" placeholder="Contoh: 55.00" value="<?php echo set_value('n_bindo'); ?>" step="0.01">
                            <?php echo form_error('n_bindo'); ?>
                        </div>
                    </div>
                    <!-- Nilai Matematika -->
                    <div class="form-group">
                        <label for="n_mat" class="col-sm-2 control-label">Nilai Matematika</label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" name="n_mat" id="n_mat" placeholder="Contoh: 55.00" value="<?php echo set_value('n_mat'); ?>" step="0.01">
                            <?php echo form_error('n_mat'); ?>
                        </div>
                    </div>
                    <!-- Nilai Bahasa Inggris -->
                    <div class="form-group">
                        <label for="n_bing" class="col-sm-2 control-label">Nilai Bahasa Inggris</label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" name="n_bing" id="n_bing" placeholder="Contoh: 55.00" value="<?php echo set_value('n_bing'); ?>" step="0.01">
                            <?php echo form_error('n_bing'); ?>
                        </div>
                    </div>
                    <!-- Nilai IPA -->
                    <div class="form-group">
                        <label for="n_peminatan" class="col-sm-2 control-label">Nilai IPA</label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" name="n_peminatan" id="n_peminatan" placeholder="Contoh: 55.00" value="<?php echo set_value('n_peminatan'); ?>" step="0.01">
                            <?php echo form_error('n_peminatan'); ?>
                        </div>
                    </div>
                    <!-- Lulus -->
                    <div class="form-group">
                        <label for="is_pass" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-3">
                            <select name="is_pass" id="is_pass" class="form-control">
                                <?php
                                $is_pass = ['lulus' => 'Lulus', 'tidak lulus' => 'Tidak Lulus'];
                                foreach ($is_pass as $key => $val) {
                                    echo "<option value='" . $key . "'>" . $val . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Simpan</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
                <?php echo form_close(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>

</section><!-- /.content -->

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->


<?php $this->load->view('template/foot'); ?>