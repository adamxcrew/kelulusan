<?php $this->load->view('template/head'); ?>

<!--tambahkan custom css disini-->

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>
                                <i class="fa fa-info-circle" aria-hidden="true"></i> Note:
                            </label>
                            <p>
                                Untuk mengimpor data siswa, silahkan tekan tombol download format data. <br>
                                <small><sup>*)</sup></small> file yang bisa di import adalah .xls (Excel 2003-2007).<br>
                                <small><sup>*)</sup></small> Jangan lupa hapus header di file excel setelah mengedit / input data, kemudian disave dan diimport.
                            </p>
                            <!-- Form Upload -->
                            <?php echo form_open_multipart(site_url('admin/siswa/upload'), ['class' => 'form-inline']); ?>
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-default">
                                        <input type="file" id="upload_file" name="upload_file" required>
                                    </span>
                                </label>
                            </div>
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-upload" aria-hidden="true"></i> Impor Data</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <a href="<?= base_url('admin/siswa/download'); ?>" class="btn btn-warning btn-block btn-flat"><i class="fa fa-download" aria-hidden="true"></i> Download Format Data</a>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <a href="<?= site_url('admin/siswa/add'); ?>" class="btn btn-primary btn-block btn-flat"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Data Siswa</a>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <a href="<?= site_url('admin/siswa/truncate'); ?>" class="btn btn-danger btn-block btn-flat" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash" aria-hidden="true"></i> Hapus Semua Data</a>
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- Datatables -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-info-circle" aria-hidden="true"></i> Data Nilai Siswa</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <?php echo $this->session->flashdata('message'); ?>
                                    <table id="example1" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Nomor Peserta</th>
                                                <th class="text-center">Nama Peserta</th>
                                                <th class="text-center">Nilai B.Indo</th>
                                                <th class="text-center">Nilai B.Ingg</th>
                                                <th class="text-center">Nilai MTK</th>
                                                <th class="text-center">Nilai IPA</th>
                                                <th class="text-center">Nilai Total</th>
                                                <th class="text-center">Nilai Rerata</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $n = 1;
                                            if (!empty($data_siswa)) :
                                                foreach ($data_siswa as $row) :
                                                    ?>
                                                    <tr>
                                                        <td class="text-center"><?= $n++; ?></td>
                                                        <td class="text-center"><?= $row['student_id']; ?></td>
                                                        <td class="text-left"><?= strtoupper($row['name']); ?></td>
                                                        <td class="text-center"><?= $row['n_bindo']; ?></td>
                                                        <td class="text-center"><?= $row['n_bing']; ?></td>
                                                        <td class="text-center"><?= $row['n_mat']; ?></td>
                                                        <td class="text-center"><?= $row['n_peminatan']; ?></td>
                                                        <td class="text-center"><?= $row['n_total']; ?></td>
                                                        <td class="text-center"><?= $row['n_rata']; ?></td>
                                                        <td class="text-center">
                                                            <div class="btn-group">
                                                                <a href="<?= site_url('admin/siswa/detail/' . $row['student_id']); ?>" class="btn btn-default btn-flat btn-sm">Detail</a>
                                                                <a href="<?= site_url('admin/siswa/edit/' . $row['student_id']); ?>" class="btn btn-primary btn-flat btn-sm">Edit</a>
                                                                <a href="<?= site_url('admin/siswa/delete/' . $row['student_id']); ?>" class="btn btn-danger btn-flat btn-sm" onclick="return confirm('Are you sure you want to delete this item?');">Hapus</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col-xs-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-xs-12 -->
    </div>
    <!-- /.row -->


</section><!-- /.content -->

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->

<!-- page script -->
<script>
    $(document).ready(function() {
        $('#example1').DataTable();
    });

    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>

<?php $this->load->view('template/foot'); ?>