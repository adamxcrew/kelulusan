<?php $this->load->view('template/head'); ?>

<!--tambahkan custom css disini-->

<style type="text/css">
    #user_profile {
        width: 128px;
        height: 128px;
        display: block;
        margin-left: auto;
        margin-right: auto
    }
</style>

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-info-circle" aria-hidden="true"></i> Detail Siswa</h3>
                </div>
                <!-- /.box-header -->
                <!-- Profile Image -->
                <div class="box box-default">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" id="user_profile" src="<?= base_url(); ?>assets/AdminLTE-2.0.5/dist/img/icon-user.png" alt="User profile picture">

                        <h3 class="profile-username text-center"><?= strtoupper($profile[0]['name']); ?></h3>

                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>Nomor Peserta</b> <a class="pull-right"><?= $profile[0]['student_id']; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <?php
                                        if ($profile[0]['gender'] == 'L') $jenkel = 'Laki-laki';
                                        else $jenkel = 'Perempuan';
                                        ?>
                                        <b>Jenis Kelamin</b> <a class="pull-right"><?= $jenkel; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Tingkat Pendidikan</b> <a class="pull-right">SMP/MTs</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Nomor Telepon</b> <a class="pull-right"><?= $profile[0]['telp']; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Tanggal Lahir</b> <a class="pull-right"><?= date('d F Y', strtotime($profile[0]['birthday'])); ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <?php
                                        if ($profile[0]['is_pass'] == 'lulus') $is_pass = '<span class="label label-success">Lulus</span>';
                                        else $is_pass = '<span class="label label-danger">Tidak Lulus</span>';
                                        ?>
                                        <b>Status Kelulusan</b> <a class="pull-right"><?= $is_pass; ?></a>
                                    </li>
                                </ul>
                            </div>
                            <!-- Nilai -->
                            <div class="col-md-6 col-sm-12">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>Nilai Bahasa Indonesia</b> <a class="pull-right"><?= $profile[0]['n_bindo']; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Nilai Bahasa Inggris</b> <a class="pull-right"><?= $profile[0]['n_bing']; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Nilai Matematika</b> <a class="pull-right"><?= $profile[0]['n_mat']; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Nilai IPA</b> <a class="pull-right"><?= $profile[0]['n_peminatan']; ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Total Nilai</b> <a class="pull-right"><?= round($profile[0]['n_total'], 2); ?></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Rata-rata Nilai</b> <a class="pull-right"><?= round($profile[0]['n_rata'], 2); ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <button type="button" id="kembali" class="btn btn-flat btn-primary">
                            <i class="fa fa-arrow-circle-left"></i>&nbsp;Kembali
                        </button>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.box -->

        </div>
    </div>

</section><!-- /.content -->

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->
<script type="text/javascript">
    $(document).ready(function() {
        $('#kembali').on('click', function() {
            window.history.back();
        });
    });
</script>

<?php $this->load->view('template/foot'); ?>