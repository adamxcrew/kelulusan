<?php $this->load->view('template/head'); ?>

<!--tambahkan custom css disini-->
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" type="text/css" />

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <?= $this->session->flashdata('message'); ?>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-clock-o"></i>&nbsp;Jadwal Pengumuman</h3>
                </div>
                <div class="box-body">
                    <?php if ($jadwal->num_rows() < 1) : ?>
                        <div class="well well-sm">
                            <h3><i class="fa fa-info-circle"></i>&nbsp;Jadwal pengumuman belum diatur!</h3>
                        </div>
                    <?php else : ?>
                        <div class="well well-sm">
                            <?php $jadwal = $jadwal->result_array(); ?>
                            <p>
                                Tanggal Pengumuman : <strong><?= date('d F Y', strtotime($jadwal[0]['start_date'])); ?>&nbsp;
                                    Pukul <?= date('H:i', strtotime($jadwal[0]['start_date'])); ?> WIB</strong>.
                            </p>
                        </div>
                    <?php endif; ?>

                    <?= form_open('admin/jadwal', ['class' => 'form-inline']); ?>
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-xs-12">
                            <!-- Date -->
                            <label for="date"><i class="fa fa-calendar"></i> Atur Jadwal Pengumuman :</label>
                            <div class="input-append date form_datetime">
                                <input size="16" type="text" name="jadwal" class="form-control" value="">
                                <span class="add-on"><i class="icon-th"></i></span>
                                <button type="submit" name="btn-jadwal" class="btn btn-flat btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-xs-12 -->
    </div>
    <!-- /.row -->


</section><!-- /.content -->

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->
<script src="<?= base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript">
    $(function() {
        $(".form_datetime").datetimepicker({
            format: "yyyy-mm-dd hh:ii",
            autoclose: true,
            todayBtn: true,
            pickerPosition: "bottom-left"
        });
    });
</script>

<?php $this->load->view('template/foot'); ?>