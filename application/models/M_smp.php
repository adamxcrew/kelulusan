<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_smp extends CI_Model
{

	public function getAll($table)
	{
		return $this->db->get($table);
	}

	public function getDetailSiswa($table, $id)
	{
		return $this->db->get_where($table, $id);
	}

	public function setQuery($query)
	{
		return $this->db->query($query);
	}

	public function get_data_siswa($table, $column, $params)
	{
		$this->db->order_by($column, $params);
		$query = $this->db->get($table);

		return $query;
	}

	public function delete($table, $column, $value)
	{
		$this->db->where($column, $value);
		$query = $this->db->delete($table);

		return $query;
	}

	public function delete_all($table)
	{
		$query = $this->db->empty_table($table);

		return $query;
	}
}
